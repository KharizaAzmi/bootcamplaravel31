<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function biodata(){
        return view('halaman.form');
    }
    public function send(Request $request){
        // dd($request->all());
        $firstname = $request["fname"];
        $lastname = $request["lname"];

        return view('welcome', compact('firstname', 'lastname'));
    }
}
