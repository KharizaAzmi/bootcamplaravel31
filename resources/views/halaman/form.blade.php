<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
      @csrf
      <label for="fname">First name:</label><br />
      <input type="text" id="fname" name="fname" /><br />
      <label for="lname">Last name:</label><br />
      <input type="text" id="lname" name="lname" />
      <p>Gender:</p>
      <input type="radio" id="male" name="gender" value="Male" />
      <label for="male">Male</label><br />
      <input type="radio" id="female" name="gender" value="Female" />
      <label for="female">Female</label><br />
      <input type="radio" id="other" name="gender" value="Other" />
      <label for="other">Other</label><br />
      <br />
      <label>Nationality</label><br />
      <br />
      <select>
        <option value="Indonesian">Indonesian</option>
        <option value="Singapuran">Singapuran</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Australian">Australian</option>
      </select>
      <br />
      <br />
      <label>Language Spoken:</label><br />
      <br />
      <input type="checkbox" id="bahasa1" name="bahasa1" value="Indonesia" />
      <label for="bahasa1"> Bahasa Indonesia</label><br />
      <input type="checkbox" id="bahasa2" name="bahasa2" value="English" />
      <label for="bahasa2"> English</label><br />
      <input type="checkbox" id="bahasa3" name="bahasa3" value="Other" />
      <label for="bahasa3"> Other</label><br />
      <br />
      <label>Bio:</label><br />
      <br />
      <textarea name="bip" rows="10" cols="30"> </textarea>
      <br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
